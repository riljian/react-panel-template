import React, { Component } from 'react'
import { Switch, Route, Link, NavLink } from 'react-router-dom'
import Index from '../../routes/Index'
import NoMatch from '../../routes/NoMatch'
import './index.css'

class App extends Component {
  render() {
    return (
      <div className="app flex flex-wrap">
        <header className="bg-dark shadow">
          <Link to="/" className="app-brand">Dynacolor Panel</Link>
        </header>
        <nav className="bg-light flex flex-column">
          <NavLink to="/no-match" activeClassName="active">No Match</NavLink>
        </nav>
        <main className="flex-grow">
          <Switch>
            <Route exact path="/" component={Index} />
            <Route component={NoMatch} />
          </Switch>
        </main>
      </div>
    )
  }
}

export default App
