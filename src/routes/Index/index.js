import React, { Component } from 'react'

class Index extends Component {
  render() {
    return (
      <div className="index">
        <h1>Index</h1>
      </div>
    )
  }
}

export default Index
